<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 26/07/17
 * Time: 18:11
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model {
    
    protected $table = 'asignacion';

    protected $fillable = [
        'usuario_id',
        'curso_id',
        'anio',
        'trimestre',
        'nota',
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

}