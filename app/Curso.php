<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 26/07/17
 * Time: 18:10
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Curso extends Model {
    
    protected $table = 'curso';

    protected $fillable = [
        'nombre',
    ];

    public function asignaciones()
    {
        return $this->hasMany(Asignacion::class);
    }
    
}