<?php

namespace App\Http\Controllers;

use App\Asignacion;
use App\Respuesta;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class NotasController extends Controller {

    public function login(Request $request)
    {
        $respuesta = new Respuesta();
        if ($request->has('correo') && $request->has('password')) {
            $usuario = User::where('correo', $request->correo)->first();
            if ($usuario == null) {
                $respuesta->codigo_error = 2;
                $respuesta->mensaje_error = "Usuario no existe";
            } else {
                if (Hash::check($request->password, $usuario->password)) {
                    $respuesta->respuesta = true;
                    $respuesta->usuario = $usuario;
                } else {
                    $respuesta->codigo_error = 3;
                    $respuesta->mensaje_error = "Credenciales inválidas";
                }
            }
        } else {
            $respuesta->codigo_error = 1;
            $respuesta->mensaje_error = "No trae los parametros necesarios";
        }
        return response()->json($respuesta);
    }

    public function notas(Request $request, $usuario, $anio, $trimestre)
    {
        $respuesta = new Respuesta();
        $respuesta->respuesta = true;
        $respuesta->asignaturas = Asignacion::where('usuario_id', $usuario->id)
            ->where('anio', $anio)
            ->where('trimestre', $trimestre)
            ->with('curso')
            ->get();
        return response()->json($respuesta);
    }
}
