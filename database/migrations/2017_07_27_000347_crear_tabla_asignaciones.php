<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAsignaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('curso_id')->unsigned();
            $table->integer('anio');
            $table->integer('trimestre');
            $table->timestamps();
            $table->foreign('usuario_id')
                ->references('id')->on('usuario')
                ->onDelete('cascade');
            $table->foreign('curso_id')
                ->references('id')->on('curso')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignacion');
    }
}
