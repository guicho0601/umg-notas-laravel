<?php

use App\Asignacion;
use App\Curso;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario1 = User::create(['nombre' => 'Luis', 'apellido' => 'Ramos', 'correo' => 'lramosg1@miumg.edu.gt', 'password' => Hash::make('1234')]);
        $usuario2 = User::create(['nombre' => 'Joseph', 'apellido' => 'Ajcan', 'correo' => 'jajcanb@miumg.edu.gt', 'password' => Hash::make('1234')]);
        $usuario3 = User::create(['nombre' => 'Mario', 'apellido' => 'Godoy', 'correo' => 'mgodoyt@miumg.edu.gt', 'password' => Hash::make('1234')]);
        $usuario4 = User::create(['nombre' => 'Eduardo', 'apellido' => 'Palencia', 'correo' => 'epalenciat@miumg.edu.gt', 'password' => Hash::make('1234')]);
        $usuario5 = User::create(['nombre' => 'Kevin', 'apellido' => 'Cruz', 'correo' => 'kcruz@miumg.edu.gt', 'password' => Hash::make('1234')]);
        $usuarios = collect([$usuario1, $usuario2, $usuario3, $usuario4, $usuario5]);
        $curso1 = Curso::create(['nombre' => 'Ética']);
        $curso2 = Curso::create(['nombre' => 'Estadística computacional']);
        $curso3 = Curso::create(['nombre' => 'Sistemas de información']);
        $curso4 = Curso::create(['nombre' => 'Almacenamiento de la información']);
        $curso5 = Curso::create(['nombre' => 'Auditoría de sistemas']);
        $curso6 = Curso::create(['nombre' => 'Seminario de investigación']);
        $curso7 = Curso::create(['nombre' => 'Programación en Java']);
        $curso8 = Curso::create(['nombre' => 'Inteligencia artificial']);
        $cursos = collect([$curso1, $curso2, $curso3, $curso4, $curso5, $curso6, $curso7, $curso8]);
        foreach ($usuarios as $usuario) {
            foreach ($cursos as $curso) {
                Asignacion::create([
                    'usuario_id' => $usuario->id,
                    'curso_id'   => $curso->id,
                    'anio'       => rand(2015, 2017),
                    'trimestre'  => rand(1, 3),
                    'nota'       => rand(50, 90),
                ]);
            }
        }
    }
}
