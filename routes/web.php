<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::bind('usuario', function ($id){
    return \App\User::findOrFail($id);
});

Route::post('login', 'NotasController@login');
Route::post('notas/{usuario}/{anio}/{trimestre}', 'NotasController@notas');
